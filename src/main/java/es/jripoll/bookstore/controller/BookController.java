package es.jripoll.bookstore.controller;

import java.util.Calendar;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import es.jripoll.bookstore.domain.Book;
import es.jripoll.bookstore.service.BookService;

@Controller
@RequestMapping("/book")
public class BookController {

	@Autowired
	private BookService bookService;
	
	@ModelAttribute("allBooks")
	public List<Book> populateBooks(){
		return this.bookService.findAllBooks();
	}
	
	@GetMapping(value = {"/", "/list"})
	public String showBooks () {
		
		return "book";
	}
	
	@GetMapping(value = "/create")
	public String createBook (Book book) {
		return "newbook";
	}
	
	@GetMapping(value = "/{id}")
	public String editBoook (@PathVariable("id") final String id, Model model) {
		Book book = bookService.findBookByID(Long.parseLong(id));
		model.addAttribute("book", book);
		return "editbook";
	}
	
	@PostMapping(value = "/saveNewBook", params="saveNewBook")
	public String saveNewBook (@Valid Book book, final BindingResult bindingResult, final ModelMap model) {
		 if (bindingResult.hasErrors()) {
		      return "newbook";
		    }
		 	book.setPublishedDate(Calendar.getInstance().getTime());
		    this.bookService.saveBook(book);
		    model.clear();
		return "redirect:/book/";
	}
	
	@PostMapping(value = "/saveEditedBook", params="saveEditedBook")
	public String saveEditedBook (@Valid Book book, final BindingResult bindingResult, final ModelMap model) {
		 if (bindingResult.hasErrors()) {
		      return "editbook";
		    }
		 	book.setPublishedDate(Calendar.getInstance().getTime());
		    this.bookService.saveBook(book);
		    model.clear();
		return "redirect:/book/";
	}
	
	@PostMapping(value = "/delete", params="delete")
	public String deleteBook (final String id) {
		bookService.deleteBook(Long.parseLong(id));
		return "redirect:/book/";
	}
	
}
