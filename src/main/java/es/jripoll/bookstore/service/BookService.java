package es.jripoll.bookstore.service;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.jripoll.bookstore.domain.Book;
import es.jripoll.bookstore.repository.BookRepository;

@Service
public class BookService {

	@Autowired
	private BookRepository bookRepository;
	
	//Recoger todos los libros
	public List<Book> findAllBooks() {
		return this.bookRepository.findAll();
	}
	
	//Recoger libro por ID
	public Book findBookByID (final Long id) {
		return this.bookRepository.findById(id).orElseThrow(NoSuchElementException::new);
	}
	
	//Guardar libro
	public void saveBook (Book book) {
		this.bookRepository.save(book);
	}
	
	//Eliminar libro
	public void deleteBook (final Long id) {
			this.bookRepository.deleteById(id);
	}
	
	//Actualizar libro
	public void updateBook(final Long id, Book book) {
		Book newBook = this.bookRepository.findById(id).orElseThrow(NoSuchElementException::new);
		newBook.setTitle(book.getTitle());
		newBook.setISBN(book.getISBN());
		newBook.setPrice(book.getPrice());
		newBook.setAuthor(book.getAuthor());
		newBook.setPublishedDate(book.getPublishedDate());
		newBook.setEditorial(book.getEditorial());
		newBook.setIsOnSale(book.getIsOnSale());
		bookRepository.save(newBook);
	}
	
}
