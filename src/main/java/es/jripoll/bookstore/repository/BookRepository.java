package es.jripoll.bookstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.jripoll.bookstore.domain.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
	
}
