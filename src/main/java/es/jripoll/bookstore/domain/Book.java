package es.jripoll.bookstore.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;

import org.hibernate.validator.constraints.ISBN;
import org.hibernate.validator.constraints.ISBN.Type;

@Entity
public class Book implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotEmpty(message="El campo titulo es obligatorio")
	private String title;
	
	@ISBN(type = Type.ISBN_13)
	private String ISBN;
	
	@DecimalMin(value = "0.0")
    @Digits(fraction=2, integer = 3)
	@NotNull(message = "El campo precio es obligatorio")
	private BigDecimal price;
	
	@NotEmpty(message="El campo autor es obligatorio")
	private String author;
	
	@CreatedDate
	private Date publishedDate;
	
	@NotEmpty(message="El campo editorial es obligatorio")
	private String editorial;
	
	private boolean isOnSale;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(Date publishedDate) {
		this.publishedDate = publishedDate;
	}

	public String getEditorial() {
		return editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	public boolean getIsOnSale() {
		return isOnSale;
	}

	public void setIsOnSale(boolean isOnSale) {
		this.isOnSale = isOnSale;
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", title=" + title + ", ISBN=" + ISBN + ", price=" + price + ", author=" + author
				+ ", publishedDate=" + publishedDate + ", editorial=" + editorial + ", isOnSale=" + isOnSale + "]";
	}
	
}
